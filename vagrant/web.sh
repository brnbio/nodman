#!/usr/bin/env bash

PHP_VERSION=7.4

# install ansible
sudo apt-get update
sudo apt-get -y install ansible

# setup host
sudo rm /etc/nginx/sites-enabled/default
sudo rm /etc/nginx/sites-available/default
echo "server {
	listen 80;
	listen [::]:80;
	root /var/www/html/public/;
	index index.php;
	location / {
		try_files \$uri /index.php?\$args;
	}
	location ~ \.php\$ {
		try_files \$uri =404;
		include /etc/nginx/fastcgi_params;
		fastcgi_pass   unix:/run/php/php$PHP_VERSION-fpm.sock;
		fastcgi_index  index.php;
		fastcgi_param  SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
	}
}" | sudo tee /etc/nginx/sites-available/default
sudo ln -s /etc/nginx/sites-available/default /etc/nginx/sites-enabled/default
sudo service nginx restart

# php version
sudo update-alternatives --set php /usr/bin/php$PHP_VERSION
sudo update-alternatives --set phar /usr/bin/phar$PHP_VERSION
sudo update-alternatives --set phar.phar /usr/bin/phar.phar$PHP_VERSION

# aliases
echo "alias pa=\"php artisan\"" >> /home/vagrant/.bash_aliases
