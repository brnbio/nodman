<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateCorePlaybooksTasksTable
 */
class CreateCorePlaybooksTasksTable extends Migration
{
    /**
     * @return void
     */
    public function up(): void
    {
        Schema::create('core_playbooks_tasks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid');
            $table->string('name');
            $table->string('type');
            $table->json('options')->nullable();
            $table->unsignedBigInteger('playbook_id');
            $table->integer('sort')->default(0);
            $table->timestamps();
        });
    }

    /**
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('core_playbooks_tasks');
    }
}
