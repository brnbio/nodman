<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateCoreNodesTable
 */
class CreateCoreNodesTable extends Migration
{
    /**
     * @return void
     */
    public function up(): void
    {
        Schema::create('core_nodes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid');
            $table->string('name');
            $table->string('provider');
            $table->string('provider_type');
            $table->string('ip_address');
            $table->string('os');
            $table->timestamps();
        });
    }

    /**
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('core_nodes');
    }
}
