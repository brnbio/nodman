<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateCorePlaybooksTable
 */
class CreateCorePlaybooksTable extends Migration
{
    /**
     * @return void
     */
    public function up(): void
    {
        Schema::create('core_playbooks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid');
            $table->string('name');
            $table->json('vars')->nullable();
            $table->string('remote_user')->default('root');
            $table->timestamps();
        });
    }

    /**
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('core_playbooks');
    }
}
