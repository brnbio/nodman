<?php

declare(strict_types=1);

use App\Models\Playbook;
use Illuminate\Database\Seeder;

/**
 * Class CorePlaybooksSeeder
 */
class CorePlaybooksSeeder extends Seeder
{
    /**
     * @return void
     * @throws Exception
     */
    public function run(): void
    {
        $playbook = new Playbook([
            Playbook::ATTRIBUTE_NAME => 'webserver',
        ]);
        $playbook->save();

        $task = new Playbook\Task([
            Playbook\Task::ATTRIBUTE_NAME => 'install nginx',
            Playbook\Task::ATTRIBUTE_TYPE => 'apt',
            Playbook\Task::ATTRIBUTE_OPTIONS => [
                'name' => 'nginx',
                'state' => 'present',
                'update_cache' => true,
            ],
        ]);
        $playbook->tasks()->save($task);

    }
}
