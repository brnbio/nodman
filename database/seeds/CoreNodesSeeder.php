<?php

declare(strict_types=1);

use App\Models\Node;
use Illuminate\Database\Seeder;

/**
 * Class CoreNodesSeeder
 */
class CoreNodesSeeder extends Seeder
{
    /**
     * @return void
     * @throws Exception
     */
    public function run(): void
    {
        for ($i = 1; $i <= 2; $i++) {
            $node = new Node([
                'name' => 'node' . $i,
                'provider' => 'custom',
                'provider_type' => '',
                'ip_address' => '192.168.'. (30 + $i) .'.16',
                'os' => 'Ubuntu 18.04.1 LTS',
            ]);
            $node->save();
        }
    }
}
