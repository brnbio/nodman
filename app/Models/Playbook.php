<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Playbook
 * @package App\Models
 */
class Playbook extends AppModel
{
    public const TABLE = 'core_playbooks';
    public const ATTRIBUTE_NAME = 'name';

    /**
     * @var array
     */
    protected $fillable = [
        self::ATTRIBUTE_NAME,
    ];

    /**
     * @return HasMany
     */
    public function tasks(): HasMany
    {
        return $this->hasMany(
            Playbook\Task::class,
            Playbook\Task::ATTRIBUTE_PLAYBOOK_ID,
        );
    }

    /**
     * @return string
     */
    public function run(): string
    {
        // generate ansible.xml > generate job
        // run playbook > run job

        return '';
    }
}
