<?php

declare(strict_types=1);

namespace App\Models\Playbook;

use App\Models\AppModel;

/**
 * Class Task
 * @package App\Models\Playbook
 */
class Task extends AppModel
{
    public const TABLE = 'core_playbooks_tasks';

    public const ATTRIBUTE_NAME = 'name';
    public const ATTRIBUTE_TYPE = 'type';
    public const ATTRIBUTE_OPTIONS = 'options';
    public const ATTRIBUTE_PLAYBOOK_ID = 'playbook_id';
    public const ATTRIBUTE_SORT = 'sort';

    /**
     * @var array
     */
    protected $fillable = [
        self::ATTRIBUTE_NAME,
        self::ATTRIBUTE_TYPE,
        self::ATTRIBUTE_OPTIONS,
    ];

    /**
     * @var array
     */
    protected $casts = [
        self::ATTRIBUTE_OPTIONS => 'json',
    ];

}
