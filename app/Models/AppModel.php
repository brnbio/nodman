<?php

declare(strict_types=1);

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Ramsey\Uuid\Uuid;

/**
 * Class AppModel
 * @package App\Models
 */
class AppModel extends Model
{
    public const ATTRIBUTE_ID = 'id';
    public const ATTRIBUTE_UUID = 'uuid';

    public int $id;
    public string $uuid;
    public Carbon $created_at;
    public Carbon $updated_at;

    /**
     * AppModel constructor.
     * @param array $attributes
     * @throws Exception
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        if (! $this->exists) {
            $this->setAttribute(self::ATTRIBUTE_UUID, Uuid::uuid4()->toString());
        }
    }

    /**
     * @return string
     */
    public function getTable(): string
    {
        return static::TABLE;
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return $this
     */
    public function setAttribute($key, $value): self
    {
        $this->{$key} = $value;

        return parent::setAttribute($key, $value);
    }
}
