<?php

declare(strict_types=1);

namespace App\Models;

/**
 * Class Node
 * @package App\Models
 */
class Node extends AppModel
{
    public const TABLE = 'core_nodes';
    public const ATTRIBUTE_NAME = 'name';
}
